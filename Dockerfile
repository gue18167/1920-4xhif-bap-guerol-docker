FROM openjdk:8
COPY . jenkins.war
WORKDIR jenkins.war
CMD ["java", "-jar", "jenkins.war"]