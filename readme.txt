einmal docker mit jenkins erstellen: 
cmd -> cd C:\container

dockerfile mit -> startet jenkins.war
FROM openjdk:8 
COPY . jenkins.war
WORKDIR jenkins.war
CMD ["java", "-jar", "jenkins.war"]

docker build -t bap:1 . -> build with docker
docker container run -it -p 8080:8080 bap:1 -> runs docker container

localhost:8080 im Browser starten und mit Admin und Passwort einloggen

2nd run:
docker container ls 
name priceless_perlman
docker start priceless_perlman
